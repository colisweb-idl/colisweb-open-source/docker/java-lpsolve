FROM adoptopenjdk/openjdk14:jdk-14.0.2_12-slim

ARG VCS_REF

LABEL org.label-schema.vcs-ref=$VCS_REF


ENV LP_SOLVE_VERSION="5.5.2.5"
ENV LP_SOLVE_LIBRARY_NAME="liblpsolve55"
ENV LP_SOLVE_LINUX_URL="https://sourceforge.net/projects/lpsolve/files/lpsolve/$LP_SOLVE_VERSION/lp_solve_${LP_SOLVE_VERSION}_java.zip"
ENV LP_SOLVE_LINUX_URL="https://s3-eu-west-1.amazonaws.com/colisweb-libraries/lpsolve/$LP_SOLVE_VERSION/lp_solve_${LP_SOLVE_VERSION}_java.zip"
ENV LD_LIBRARY_PATH=/usr/lib/lp_solve


RUN apt-get update && \
    apt-get install -y wget unzip && \
    apt-get install -y lp-solve ${LP_SOLVE_LIBRARY_NAME}-dev

#cd /tmp

RUN wget "$LP_SOLVE_LINUX_URL" && \
    unzip lp_solve_${LP_SOLVE_VERSION}_java.zip && rm lp_solve_${LP_SOLVE_VERSION}_java.zip && \
    cp lp_solve_*/lib/ux64/${LP_SOLVE_LIBRARY_NAME}j.so /usr/lib/lp_solve && \
    rm -rf lp_solve_* && \
    chmod 755 /usr/lib/lp_solve/${LP_SOLVE_LIBRARY_NAME}j.so

