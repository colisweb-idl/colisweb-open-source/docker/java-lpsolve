[![](https://images.microbadger.com/badges/image/colisweb/java-lpsolve.svg)](https://microbadger.com/images/colisweb/java-lpsolve "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/colisweb/java-lpsolve.svg)](https://microbadger.com/images/colisweb/java-lpsolve "Get your own version badge on microbadger.com")

lpsolve library in the same docker image as Java


Published on <https://hub.docker.com/r/colisweb/java-lpsolve>

Get it with

    docker pull colisweb/java-lpsolve:1.0
